package tfe.lab5.bookstore.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tfe.lab5.bookstore.core.model.Book;
import tfe.lab5.bookstore.core.model.Client;
import tfe.lab5.bookstore.core.repository.ClientRepository;

import java.util.List;

@Service
public class ClientServiceImpl implements ClientService {
    public static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public List<Client> getAllClients() {
        log.trace("getAllClients --- method entered");

        List<Client> result = clientRepository.findAll();

        log.trace("getAllClients: result={}", result);

        return result;
    }

    @Override
    public Client saveClient(Client client) {
        log.trace("saveClient --- method entered");

        Client result = clientRepository.save(client);

        log.trace("saveClient: result={}", result);
        return result;
    }

    @Override
    public void deleteClient(int id) {
        log.trace("deleteClient --- method entered");

        clientRepository.deleteById(id);
    }

    @Override
    @Transactional
    public Client updateClient(int id, Client client) {
        log.trace("updateClient --- method entered");

        Client update = clientRepository.findById(id).orElseThrow();
        update.setName(client.getName());
        update.setDateOfRegistration(client.getDateOfRegistration());

        return update;
    }

}
