package tfe.lab5.bookstore.core.model;

import lombok.*;

import javax.persistence.Entity;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@Builder
public class Book extends BaseEntity<Integer> {
    private String name;
    private String author;
    public String publisher;
    private String yearOfPublication;
    private double price;

    public Book(int id, String name, String author, String publisher, String yearOfPublication, double price) {
        super( id );
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.yearOfPublication = yearOfPublication;
        this.price = price;
    }
}
