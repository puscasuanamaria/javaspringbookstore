package tfe.lab5.bookstore.core.service;

import tfe.lab5.bookstore.core.model.Client;

import java.util.List;

public interface ClientService {
    List<Client> getAllClients();

    Client saveClient(Client client);

    void deleteClient(int id);

    Client updateClient(int id, Client client);
}
