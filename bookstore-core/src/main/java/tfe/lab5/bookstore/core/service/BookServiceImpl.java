package tfe.lab5.bookstore.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tfe.lab5.bookstore.core.model.Book;
import tfe.lab5.bookstore.core.repository.BookRepository;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    public static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);

    @Autowired
    private BookRepository bookRepository;

    @Override
    public List<Book> getAllBooks() {
        log.trace("getAllBooks --- method entered");

        List<Book> result = bookRepository.findAll();

        log.trace("getAllBooks: result={}", result);

        return result;
    }

    @Override
    public Book saveBook(Book book) {
        log.trace("saveBook --- method entered");

        Book result = bookRepository.save(book);

        log.trace("saveBook: result={}", result);
        return result;
    }

    @Override
    public void deleteBook(int id) {
        log.trace("deleteBook --- method entered");

        bookRepository.deleteById(id);
    }

    @Override
    @Transactional
    public Book updateBook(int id, Book book) {
        log.trace("updateBook --- method entered");

        Book update = bookRepository.findById(id).orElseThrow();
        update.setName(book.getName());
        update.setAuthor(book.getAuthor());
        update.setPublisher(book.getPublisher());
        update.setYearOfPublication(book.getYearOfPublication());
        update.setPrice(book.getPrice());

        return update;
    }
}
