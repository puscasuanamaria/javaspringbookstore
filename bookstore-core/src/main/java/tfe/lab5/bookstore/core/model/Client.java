package tfe.lab5.bookstore.core.model;

import lombok.*;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@Builder
public class Client extends BaseEntity<Integer> {
    private String name;
    private String dateOfRegistration;
}
