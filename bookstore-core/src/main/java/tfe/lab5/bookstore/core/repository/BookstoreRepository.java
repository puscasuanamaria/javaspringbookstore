package tfe.lab5.bookstore.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import tfe.lab5.bookstore.core.model.BaseEntity;

import java.io.Serializable;

@NoRepositoryBean
public interface BookstoreRepository<T extends BaseEntity<ID>,
        ID extends Serializable>
        extends JpaRepository<T, ID> {
}
