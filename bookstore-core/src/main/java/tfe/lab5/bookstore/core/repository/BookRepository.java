package tfe.lab5.bookstore.core.repository;

import tfe.lab5.bookstore.core.model.Book;

public interface BookRepository extends BookstoreRepository<Book, Integer>{
}
