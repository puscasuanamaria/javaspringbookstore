package tfe.lab5.bookstore.core.service;

import tfe.lab5.bookstore.core.model.Book;

import java.util.List;

public interface BookService {
    List<Book> getAllBooks();

    Book saveBook(Book book);

    void deleteBook(int id);

    Book updateBook(int id, Book book);
}
