package tfe.lab5.bookstore.core.repository;

import tfe.lab5.bookstore.core.model.Client;

public interface ClientRepository extends BookstoreRepository<Client, Integer>{
}
