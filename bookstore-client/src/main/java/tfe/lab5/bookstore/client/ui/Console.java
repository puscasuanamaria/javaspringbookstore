package tfe.lab5.bookstore.client.ui;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import tfe.lab5.bookstore.web.dto.BookDto;
import tfe.lab5.bookstore.web.dto.BooksDto;
import tfe.lab5.bookstore.web.dto.ClientDto;
import tfe.lab5.bookstore.web.dto.ClientsDto;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

@Component
public class Console {
    private Scanner scanner;

    @Autowired
    private RestTemplate restTemplate;

    public Console(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.scanner = new Scanner(System.in);
    }

    public void showMenu() {
        System.out.println("1. Book CRUD");
        System.out.println("2. Client CRUD");

        System.out.println("x. Exit");
    }

    public void runConsole() throws IOException {
        showMenu();
        while (true) {
            String option = scanner.next();
            switch (option) {
                case "1":
                    runBookCRUD();
                    break;
                case "2":
                    runClientCRUD();
                    break;
                case "x":
                    return;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }
    }


    private void runBookCRUD() throws IOException {
        while (true) {
            System.out.println();
            System.out.println("1. Add a book");
            System.out.println("2. View all books");
            System.out.println("3. Delete a book by a given id");
            System.out.println("4. Update a book");
            System.out.println("5. Back");

            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String option = bufferRead.readLine();
            switch (option) {
                case "1":
                    addBook();
                    break;
                case "2":
                    printAllBooks();
                    break;
                case "3":
                    deleteBookById();
                    break;
                case "4":
                    updateBook();
                    break;
                case "5":
                    return;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }
    }


    private void addBook() throws IOException {

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter title: ");
        String name = bufferRead.readLine();
        System.out.println("Enter author: ");
        String author = bufferRead.readLine();
        System.out.println("Enter publisher: ");
        String publisher = bufferRead.readLine();
        System.out.println("Enter year of publication: ");
        String yearOfPublication = bufferRead.readLine();
        System.out.println("Enter price: ");
        double price = Double.parseDouble(bufferRead.readLine());

        BookDto bookDto = new BookDto(name, author, publisher, yearOfPublication, price);
        //System.out.println(bookDto);

        String url = "http://localhost:8080/api/books";

        BookDto savedBook = restTemplate.postForObject(
                url,
                bookDto,
                BookDto.class);
        System.out.println("saved book: " + savedBook);
    }

    private void printAllBooks() {
        String url = "http://localhost:8080/api/books";
        BooksDto books = restTemplate.getForObject(url, BooksDto.class);

        books.getBooks()
                .forEach(System.out::println);
    }

    private void deleteBookById() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter id: ");
        int id = Integer.parseInt(bufferRead.readLine());

        String url = "http://localhost:8080/api/books";

        restTemplate.delete(url + "/{id}", id);

        System.out.println("after delete:");
        BooksDto allBooks = restTemplate.getForObject(url, BooksDto.class);
        allBooks.getBooks()
                .forEach(System.out::println);
    }


    private void updateBook() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter id: ");
        int id = Integer.parseInt(bufferRead.readLine());
        System.out.println("Enter title: ");
        String name = bufferRead.readLine();
        System.out.println("Enter author: ");
        String author = bufferRead.readLine();
        System.out.println("Enter publisher: ");
        String publisher = bufferRead.readLine();
        System.out.println("Enter year of publication: ");
        String yearOfPublication = bufferRead.readLine();
        System.out.println("Enter price: ");
        double price = Double.parseDouble(bufferRead.readLine());

        BookDto bookDto = new BookDto(id, name, author, publisher, yearOfPublication, price);

        String url = "http://localhost:8080/api/books";

        restTemplate.put(url + "/{id}", bookDto, bookDto.getId());
    }

    private void runClientCRUD() throws IOException {
        while (true) {
            System.out.println();
            System.out.println("1. Add a client");
            System.out.println("2. View all clients");
            System.out.println("3. Delete a client by a given id");
            System.out.println("4. Update a client");
            System.out.println("5. Back");

            BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
            String option = bufferRead.readLine();
            switch (option) {
                case "1":
                    addClient();
                    break;
                case "2":
                    printAllCllients();
                    break;
                case "3":
                    deleteClientById();
                    break;
                case "4":
                    updateClient();
                    break;
                case "5":
                    return;
                default:
                    System.out.println("Invalid option!");
                    break;
            }
        }
    }


    private void printAllCllients() {
        String url = "http://localhost:8080/api/clients";
        ClientsDto clients = restTemplate.getForObject(url, ClientsDto.class);

        clients.getClients()
                .forEach(System.out::println);
    }

    private void addClient() throws IOException {

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter name: ");
        String name = bufferRead.readLine();
        System.out.println("Enter date of registration: ");
        String dateOfRegistration = bufferRead.readLine();

        ClientDto clientDto = new ClientDto(name, dateOfRegistration);

        String url = "http://localhost:8080/api/clients";

        ClientDto savedClient = restTemplate.postForObject(
                url,
                clientDto,
                ClientDto.class);
        System.out.println("saved client: " + savedClient);
    }

    private void deleteClientById() throws IOException {
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter id: ");
        int id = Integer.parseInt(bufferRead.readLine());

        String url = "http://localhost:8080/api/clients";

        restTemplate.delete(url + "/{id}", id);

        System.out.println("after delete:");
        ClientsDto allClients = restTemplate.getForObject(url, ClientsDto.class);
        allClients.getClients()
                .forEach(System.out::println);
    }

    private void updateClient() throws IOException {

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));

        System.out.println("Enter id: ");
        int id = Integer.parseInt(bufferRead.readLine());
        System.out.println("Enter name: ");
        String name = bufferRead.readLine();
        System.out.println("Enter date of registration: ");
        String dateOfRegistration = bufferRead.readLine();

        ClientDto clientDto = new ClientDto(id, name, dateOfRegistration);

        String url = "http://localhost:8080/api/clients";

        restTemplate.put(url + "/{id}", clientDto, clientDto.getId());
    }

}