package tfe.lab5.bookstore.client;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import tfe.lab5.bookstore.client.ui.Console;

import java.io.IOException;

public class ClientApp {
    public static void main(String[] args) throws IOException {
        AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(
                        "tfe.lab5.bookstore.client.config"
                );

        Console console = context.getBean(Console.class);

        console.runConsole();

        System.out.println("bye client");
    }
}
