package tfe.lab5.bookstore.web.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@Builder
public class BookDto extends BaseDto{
    private String name;
    private String author;
    public String publisher;
    private String yearOfPublication;
    private double price;

    @Builder
    public BookDto(int id, String name, String author, String publisher, String yearOfPublication, double price) {
        super(id);
        this.name = name;
        this.author = author;
        this.publisher = publisher;
        this.yearOfPublication = yearOfPublication;
        this.price = price;
    }
}
