package tfe.lab5.bookstore.web.dto;

import lombok.*;
import tfe.lab5.bookstore.core.model.Client;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString(callSuper = true)
@Builder
public class ClientDto extends BaseDto{
    private String name;
    private String dateOfRegistration;

    @Builder
    public ClientDto (int id, String name, String dateOfRegistration ){
        super(id);
        this.name = name;
        this.dateOfRegistration = dateOfRegistration;
    }
}
