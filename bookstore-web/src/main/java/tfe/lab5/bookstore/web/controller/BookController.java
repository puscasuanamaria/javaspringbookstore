package tfe.lab5.bookstore.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tfe.lab5.bookstore.core.model.Book;
import tfe.lab5.bookstore.core.service.BookService;
import tfe.lab5.bookstore.web.converter.BookConverter;
import tfe.lab5.bookstore.web.dto.BookDto;
import tfe.lab5.bookstore.web.dto.BooksDto;

import java.util.List;

@RestController
public class BookController {
    public static final Logger log = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    @Autowired
    private BookConverter bookConverter;

    @RequestMapping(value = "/books", method = RequestMethod.GET)
    BooksDto getBooks() {
        log.trace("getBooks --- method entered");

        List<Book> books = bookService.getAllBooks();
        BooksDto result = new BooksDto(bookConverter
                .convertModelsToDtos(books));

        return result;
    }

    @RequestMapping(value = "/books", method = RequestMethod.POST)
    BookDto saveBook(@RequestBody BookDto bookDto) {

        Book savedBook = bookService.saveBook(
                bookConverter.convertDtoToModel(bookDto));
        BookDto result = bookConverter.convertModelToDto(savedBook);
        return result;
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteBook(@PathVariable int id) {
        log.trace("deleteBook --- method entered");

        bookService.deleteBook(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/books/{id}", method = RequestMethod.PUT)
    BookDto updateBook(@PathVariable int id,
                             @RequestBody BookDto bookDto) {

        log.trace("updateBook --- method entered");

        Book book = bookService.updateBook(id, bookConverter.convertDtoToModel(bookDto));

        return bookConverter.convertModelToDto(book);
    }

}
