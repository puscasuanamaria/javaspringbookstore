package tfe.lab5.bookstore.web.converter;

import org.springframework.stereotype.Component;;
import tfe.lab5.bookstore.core.model.Client;
import tfe.lab5.bookstore.web.dto.ClientDto;

@Component
public class ClientConverter extends BaseConverter<Client, ClientDto>{
    @Override
    public Client convertDtoToModel(ClientDto dto) {
        Client client = Client.builder()
                .name(dto.getName())
                .dateOfRegistration((dto.getDateOfRegistration()))
                .build();
        client.setId(dto.getId());
        return client;
    }

    @Override
    public ClientDto convertModelToDto(Client client) {
        ClientDto dto = ClientDto.builder()
                .name(client.getName())
                .dateOfRegistration((client.getDateOfRegistration()))
                .build();
        dto.setId(client.getId());
        return dto;
    }
}
