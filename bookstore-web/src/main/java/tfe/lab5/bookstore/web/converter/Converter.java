package tfe.lab5.bookstore.web.converter;

import tfe.lab5.bookstore.core.model.BaseEntity;
import tfe.lab5.bookstore.web.dto.BaseDto;

public interface Converter<Model extends BaseEntity<Integer>, Dto extends BaseDto> {

    Model convertDtoToModel(Dto dto);

    Dto convertModelToDto(Model model);

}
