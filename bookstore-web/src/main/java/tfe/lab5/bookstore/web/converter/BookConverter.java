package tfe.lab5.bookstore.web.converter;

import org.springframework.stereotype.Component;
import tfe.lab5.bookstore.core.model.Book;
import tfe.lab5.bookstore.web.dto.BookDto;

@Component
public class BookConverter extends BaseConverter<Book, BookDto>{

    @Override
    public Book convertDtoToModel(BookDto dto) {
        Book book = Book.builder()
                .name(dto.getName())
                .author((dto.getAuthor()))
                .publisher((dto.getPublisher()))
                .yearOfPublication(dto.getYearOfPublication())
                .price(dto.getPrice())
                .build();
        book.setId(dto.getId());
        return book;
    }

    @Override
    public BookDto convertModelToDto(Book book) {
        BookDto dto = BookDto.builder()
                .name(book.getName())
                .author((book.getAuthor()))
                .publisher((book.getPublisher()))
                .yearOfPublication((book.getYearOfPublication()))
                .price((book.getPrice()))
                .build();
        dto.setId(book.getId());
        return dto;
    }
}
