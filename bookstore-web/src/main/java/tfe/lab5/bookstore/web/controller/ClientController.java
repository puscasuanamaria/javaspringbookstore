package tfe.lab5.bookstore.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import tfe.lab5.bookstore.core.model.Book;
import tfe.lab5.bookstore.core.model.Client;
import tfe.lab5.bookstore.core.service.ClientService;
import tfe.lab5.bookstore.web.converter.ClientConverter;
import tfe.lab5.bookstore.web.dto.BookDto;
import tfe.lab5.bookstore.web.dto.ClientDto;
import tfe.lab5.bookstore.web.dto.ClientsDto;

import java.util.List;

@RestController
public class ClientController {

    public static final Logger log = LoggerFactory.getLogger(ClientController.class);

    @Autowired
    private ClientService clientService;

    @Autowired
    private ClientConverter clientConverter;

    @RequestMapping(value = "/clients", method = RequestMethod.GET)
    ClientsDto getClients() {
        log.trace("getClients --- method entered");

        List<Client> clients = clientService.getAllClients();
        ClientsDto result = new ClientsDto(clientConverter
                .convertModelsToDtos(clients));

        return result;
    }

    @RequestMapping(value = "/clients", method = RequestMethod.POST)
    ClientDto saveClient(@RequestBody ClientDto clientDto) {

        Client savedClient = clientService.saveClient(
                clientConverter.convertDtoToModel(clientDto));
        ClientDto result = clientConverter.convertModelToDto(savedClient);
        return result;
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
    ResponseEntity<?> deleteClient(@PathVariable int id) {
        log.trace("deleteClient ---- method entered");

        clientService.deleteClient(id);

        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    ClientDto updateClient(@PathVariable int id,
                       @RequestBody ClientDto clientDto) {

        log.trace("updateClient --- method entered");

        Client client = clientService.updateClient(id, clientConverter.convertDtoToModel(clientDto));

        return clientConverter.convertModelToDto(client);
    }

}
